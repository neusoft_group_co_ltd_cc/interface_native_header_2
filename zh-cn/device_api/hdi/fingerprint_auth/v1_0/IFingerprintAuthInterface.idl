/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 3.2
 */

/**
 * @file IFingerprintAuthInterface.idl
 *
 * @brief 定义指纹认证驱动的执行器列表接口。此接口可用于获取驱动的执行器列表。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_0
 *
 * 引用：ohos.hdi.fingerprint_auth.v1_0.IExecutor
 *
 * @since 3.2
 */

package ohos.hdi.fingerprint_auth.v1_0;

import ohos.hdi.fingerprint_auth.v1_0.IExecutor;

/**
 * @brief 定义获取指纹认证驱动的执行器列表接口。
 *
 * @since 3.2
 * @version 1.0
 */
interface IFingerprintAuthInterface {
    /**
     * @brief 获取执行器列表，指纹认证服务进程启动进行初始化操作时通过该接口获取指纹认证驱动支持的执行器列表。
     *
     * @param executorList 执行器对象列表{@link IExecutor}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    GetExecutorList([out] IExecutor[] executorList);
}
/** @} */