/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_scene.h
 *
 * @brief Audio场景的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef AUDIO_SCENE_H
#define AUDIO_SCENE_H

#include "audio_types.h"

/**
 * @brief AudioScene音频场景接口
 *
 * 提供音频播放（render）或录音（capture）需要的公共场景驱动能力，包括选择音频场景等
 *
 * @since 1.0
 * @version 1.0
 */
struct AudioScene {
    /**
     * @brief 是否支持某个音频场景的配置
     *
     * @param handle 待操作的音频句柄
     * @param scene 待获取的音频场景描述符
     * @param supported 是否支持的状态保存到supported中，true表示支持，false表示不支持
     * @return 成功返回值0，失败返回负值
     * @see SelectScene
     */
    int32_t (*CheckSceneCapability)(AudioHandle handle, const struct AudioSceneDescriptor *scene, bool *supported);

    /**
     * @brief 选择音频场景
     *
     * <ul>
     *   <li>1. 选择一个非常具体的音频场景（应用场景和输出设备的组合），例如同样是使用手机中的喇叭作为输出设备
     *     <ul>
     *       <li>在媒体播放场景scene为media_speaker</li>
     *       <li>在语音通话免提场景scene为voice_speaker</li>
     *     </ul>
     *   <li>2. 只是选择一个音频场景，例如使用场景为媒体播放（media）、电影播放（movie）、游戏播放（game）</li>
     *   <li>3. 只是选择一个音频输出设备，例如输出设备为听筒（receiver）、喇叭（speaker）、有线耳机（headset）</li>
     * </ul>
     * @param handle 待操作的音频句柄
     * @param scene 待设置的音频场景描述符
     * @return 成功返回值0，失败返回负值
     * @see CheckSceneCapability
     */
    int32_t (*SelectScene)(AudioHandle handle, const struct AudioSceneDescriptor *scene);
};

#endif /* AUDIO_SCENE_H */
/** @} */
