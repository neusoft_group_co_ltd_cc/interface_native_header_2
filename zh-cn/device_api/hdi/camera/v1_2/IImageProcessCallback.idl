/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file iimage_process_callback.idl
 *
 * @brief 声明图像进程的回调。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：ohos.hdi.camera.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */
 

package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_2.Types;

/**
 * @brief 定义声明图像处理回调。
 *
 * 获取在流程完成时、状态已更改时、出错时的回调函数。
 *
 * @since 4.1
 * @version 1.2
 */
[callback] interface IImageProcessCallback {
    /**
     * @brief 在进程完成时调用。
     *
     * 有关报告模式的详细信息，请参阅{@link SetResultMode}。
     *
     * @param imageId 镜像ID。
     * @param buffer 缓冲区。
     *
     * @since 4.1
     * @version 1.2
     */
    OnProcessDone([in] String imageId, [in] ImageBufferInfo buffer);

    /**
     * @brief 在进程状态更改时调用。
     *
     * 有关报告模式的详细信息，请参阅{@link SetResultMode}。
     *
     * @param status 会话的新状态。
     *
     * @since 4.1
     * @version 1.2
     */
    OnStatusChanged([in] enum SessionStatus status);

    /**
     * @brief 在处理会话时发生错误时调用。
     *
     * @param imageId 镜像ID。
     * @param errorCode 错误码。
     *
     * @since 4.1
     * @version 1.2
     */
    OnError([in] String imageId, [in] int errorCode);
}
/** @} */