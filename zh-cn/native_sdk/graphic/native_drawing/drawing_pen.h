/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PEN_H
#define C_INCLUDE_DRAWING_PEN_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_pen.h
 *
 * @brief 文件中定义了与画笔相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_pen.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个画笔对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的画笔对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Pen* OH_Drawing_PenCreate(void);

/**
 * @brief 创建一个画笔对象副本{@link OH_Drawing_Pen}，用于拷贝一个已有画笔对象。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param pen 指向画笔对象的指针。
 * @return 函数会返回一个指针，指针指向创建的画笔对象副本{@link OH_Drawing_Pen}。如果对象返回NULL，表示创建失败；
 *         可能的原因是可用内存为空，或者是pen为NULL。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Pen* OH_Drawing_PenCopy(OH_Drawing_Pen* pen);

/**
 * @brief 用于销毁画笔对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenDestroy(OH_Drawing_Pen*);

/**
 * @brief 用于获取画笔是否设置抗锯齿属性，如果为真则说明画笔会启用抗锯齿功能，在绘制图形时会对图形的边缘像素进行半透明的模糊处理。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回画笔对象是否设置抗锯齿属性，返回真则设置了抗锯齿，返回假则没有设置抗锯齿。
 * @since 8
 * @version 1.0
 */
bool OH_Drawing_PenIsAntiAlias(const OH_Drawing_Pen*);

/**
 * @brief 用于设置画笔的抗锯齿属性，设置为真则画笔在绘制图形时会对图形的边缘像素进行半透明的模糊处理。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param bool 真为抗锯齿，假则不做抗锯齿处理。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetAntiAlias(OH_Drawing_Pen*, bool);

/**
 * @brief 用于获取画笔的颜色属性，颜色属性描述了画笔绘制图形轮廓时使用的颜色，用一个32位（ARGB）的变量表示。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回一个描述颜色的32位（ARGB）变量。
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_PenGetColor(const OH_Drawing_Pen*);

/**
 * @brief 用于设置画笔的颜色属性，颜色属性描述了画笔绘制图形轮廓时使用的颜色，用一个32位（ARGB）的变量表示。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param color 描述颜色的32位（ARGB）变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetColor(OH_Drawing_Pen*, uint32_t color);

/**
 * @brief 获取画笔的透明度值。画笔在勾勒图形时透明通道会使用该值。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 表示指向画笔对象的指针。
 * @return 返回一个8比特的值表示透明度。
 * @since 11
 * @version 1.0
 */
uint8_t OH_Drawing_PenGetAlpha(const OH_Drawing_Pen*);

/**

 * @brief 为画笔设置透明度值。画笔在勾勒图形时透明通道会使用该值。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 表示指向画笔对象的指针。
 * @param alpha 表示要设置的透明度值，是一个8比特的变量。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetAlpha(OH_Drawing_Pen*, uint8_t alpha);

/**
 * @brief 用于获取画笔的厚度属性，厚度属性描述了画笔绘制图形轮廓的宽度。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回画笔的厚度。
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetWidth(const OH_Drawing_Pen*);

/**
 * @brief 用于设置画笔的厚度属性，厚度属性描述了画笔绘制图形轮廓的宽度。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param width 描述画笔厚度的变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetWidth(OH_Drawing_Pen*, float width);

/**
 * @brief 用于获取折线尖角的限制值，当画笔绘制一条折线，转角类型设置为尖角时，那么此时该属性用于限制出现尖角的长度范围，如果超出则平角显示，不超出依然为尖角。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回尖角的限制值。
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetMiterLimit(const OH_Drawing_Pen*);

/**
 * @brief 用于设置折线尖角的限制值，当画笔绘制一条折线，转角类型设置为尖角时，那么此时该属性用于限制出现尖角的长度范围，如果超出则平角显示，不超出依然为尖角。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param miter 描述尖角限制值的变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetMiterLimit(OH_Drawing_Pen*, float miter);

/**
 * @brief 枚举集合定义了画笔笔帽的样式，即画笔在绘制线段时，在线段头尾端点的样式。
 * 
 * @since 8
 * @version 1.0
 */
typedef enum OH_Drawing_PenLineCapStyle {
    /** 没有笔帽样式，线条头尾端点处横切 */
    LINE_FLAT_CAP,
    /** 笔帽的样式为方框，线条的头尾端点处多出一个方框，方框宽度和线段一样宽，高度时线段厚度的一半 */
    LINE_SQUARE_CAP,
    /** 笔帽的样式为圆弧，线条的头尾端点处多出一个半圆弧，半圆的直径与线段厚度一致 */
    LINE_ROUND_CAP
} OH_Drawing_PenLineCapStyle;

/**
 * @brief 用于获取画笔笔帽的样式。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回画笔笔帽样式。
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineCapStyle OH_Drawing_PenGetCap(const OH_Drawing_Pen*);

/**
 * @brief 用于设置画笔笔帽样式。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param OH_Drawing_PenLineCapStyle 描述画笔笔帽样式的变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetCap(OH_Drawing_Pen*, OH_Drawing_PenLineCapStyle);

/**
 * @brief 枚举集合定义了线条转角的样式，即画笔在绘制折线段时，在折线转角处的样式。
 * 
 * @since 8
 * @version 1.0
 */
typedef enum OH_Drawing_PenLineJoinStyle {
    /** 转角类型为尖角，如果折线角度比较小，则尖角会很长，需要使用限制值（miter limit）进行限制 */
    LINE_MITER_JOIN,
    /** 转角类型为圆头 */
    LINE_ROUND_JOIN,
    /** 转角类型为平头 */
    LINE_BEVEL_JOIN
} OH_Drawing_PenLineJoinStyle;

/**
 * @brief 用于获取画笔绘制折线转角的样式。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @return 函数返回折线转角的样式。
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineJoinStyle OH_Drawing_PenGetJoin(const OH_Drawing_Pen*);

/**
 * @brief 用于设置画笔绘制转角的样式。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @param OH_Drawing_PenLineJoinStyle 折线转角样式。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetJoin(OH_Drawing_Pen*, OH_Drawing_PenLineJoinStyle);

/**
 * @brief 设置画笔着色器效果。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_ShaderEffect 指向着色器对象{@link OH_Drawing_ShaderEffect}的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetShaderEffect(OH_Drawing_Pen*, OH_Drawing_ShaderEffect*);

/**
 * @brief 设置画笔阴影层效果，为空表示清空阴影层效果，当前仅对文字生效。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_ShadowLayer 指向阴影层对象{@link OH_Drawing_ShadowLayer}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetShadowLayer(OH_Drawing_Pen*, OH_Drawing_ShadowLayer*);

/**
 * @brief 设置画笔路径效果。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_PathEffect 指向路径效果对象{@link OH_Drawing_PathEffect}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetPathEffect(OH_Drawing_Pen*, OH_Drawing_PathEffect*);

/**
 * @brief 设置画笔滤波器。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_Filter 指向滤波器{@link OH_Drawing_Filter}的指针，为空表示清空画笔滤波器。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PenSetFilter(OH_Drawing_Pen*, OH_Drawing_Filter*);

/**
 * @brief 从画笔获取滤波器{@link OH_Drawing_Filter}。滤波器是一个容器，可以承载蒙版滤波器和颜色滤波器。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_Filter 指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenGetFilter(OH_Drawing_Pen*, OH_Drawing_Filter*);

/**
 * @brief 为画笔设置一个混合器，该混合器实现了指定的混合模式枚举。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param OH_Drawing_BlendMode 混合模式枚举类型{@link OH_Drawing_BlendMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenSetBlendMode(OH_Drawing_Pen*, OH_Drawing_BlendMode);

/**
 * @brief 获取使用画笔绘制的源路径轮廓，并用目标路径表示。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen、src、dst任意一个为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @param src 指向源路径对象{@link OH_Drawing_Path}的指针。
 * @param dst 指向目标路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针，推荐使用NULL。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针，推荐使用NULL, 默认是一个单位矩阵。
 * @return 获取目标路径是否成功。true表示获取成功，false表示获取失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PenGetFillPath(OH_Drawing_Pen*, const OH_Drawing_Path* src, OH_Drawing_Path* dst,
    const OH_Drawing_Rect*, const OH_Drawing_Matrix*);

/**
 * @brief 将画笔重置至初始值。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Pen为NULL时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 指向画笔对象{@link OH_Drawing_Pen}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PenReset(OH_Drawing_Pen*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif