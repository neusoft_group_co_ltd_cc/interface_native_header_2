/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_EFFECT_H
#define C_INCLUDE_DRAWING_PATH_EFFECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path_effect.h
 *
 * @brief 文件中定义了与路径效果相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_path_effect.h"
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个虚线效果的路径效果对象。虚线效果由一组虚线开的间隔、虚线关的间隔数据决定。
 *
 * 本接口可能会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * intervals为NULL或者count小于等于0时返回{@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param intervals 虚线间隔数组首地址，偶数项的值表示虚线开的间隔长度，
 * 奇数项的值表示虚线关的间隔长度，单位为像素。
 * @param count 虚线间隔数组元素的个数，必须为大于0的偶数。
 * @param phase 虚线间隔数组中偏移量。
 * @return 函数返回一个指针，指针指向创建的路径效果对象{@link OH_Drawing_PathEffect}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_PathEffect* OH_Drawing_CreateDashPathEffect(float* intervals, int count, float phase);

/**
 * @brief 销毁路径效果对象并回收该对象占有内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PathEffect 指向路径效果对象{@link OH_Drawing_PathEffect}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathEffectDestroy(OH_Drawing_PathEffect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
