/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_
#define NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_

/**
 * @addtogroup NativeWindow
 * @{
 *
 * @brief 提供NativeWindow功能，作为数据生产者，可用来和egl对接。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @since 8
 * @version 1.0
 */

/**
 * @file external_window.h
 *
 * @brief 定义获取和使用NativeWindow的相关函数。
 *
 * 引用文件<native_window/external_window.h>
 * @library libnative_window.so
 * @since 8
 * @version 1.0
 */

#include <stdint.h>
#include "buffer_handle.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供对IPC序列化对象的访问功能。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OHIPCParcel OHIPCParcel;

/**
 * @brief NativeWindow结构体。
 * @since 8
 */
struct NativeWindow;

/**
 * @brief NativeWindowBuffer结构体。
 * @since 8
 */
struct NativeWindowBuffer;

/**
 * @brief 提供对OHNativeWindow的访问功能。
 * @since 8
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 提供对OHNativeWindowBuffer的访问功能。
 * @since 8
 */
typedef struct NativeWindowBuffer OHNativeWindowBuffer;

/**
 * @brief 表示本地窗口OHNativeWindow需要更新内容的矩形区域（脏区）。
 * @since 8
 */
typedef struct Region {
    /** 如果rects是空指针nullptr， 默认Buffer大小为脏区 */
    struct Rect {
        int32_t x; /**< 矩形框起始x坐标 */
        int32_t y; /**< 矩形框起始y坐标 */
        uint32_t w; /**< 矩形框宽度 */
        uint32_t h; /**< 矩形框高度 */
    } *rects;
    /** 如果rectNumber为0，默认Buffer大小为脏区 */
    int32_t rectNumber;
}Region;

/**
 * @brief 接口错误码说明（仅用于查询）。
 * @since 12
 */
typedef enum OHNativeErrorCode {
    /** 成功 */
    NATIVE_ERROR_OK = 0,
    /** 入参无效 */
    NATIVE_ERROR_INVALID_ARGUMENTS = 40001000,
    /** 无权限操作 */
    NATIVE_ERROR_NO_PERMISSION = 40301000,
    /** 无空闲可用的buffer */
    NATIVE_ERROR_NO_BUFFER = 40601000,
    /** 消费端不存在 */
    NATIVE_ERROR_NO_CONSUMER = 41202000,
    /** 未初始化 */
    NATIVE_ERROR_NOT_INIT = 41203000,
    /** 消费端已经被连接 */
    NATIVE_ERROR_CONSUMER_CONNECTED = 41206000,
    /** buffer状态不符合预期 */
    NATIVE_ERROR_BUFFER_STATE_INVALID = 41207000,
    /** buffer已在缓存队列中 */
    NATIVE_ERROR_BUFFER_IN_CACHE = 41208000,
    /** 队列已满 */
    NATIVE_ERROR_BUFFER_QUEUE_FULL = 41209000,
    /** buffer不在缓存队列中 */
    NATIVE_ERROR_BUFFER_NOT_IN_CACHE = 41210000,
    /** 当前设备或平台不支持 */
    NATIVE_ERROR_UNSUPPORT = 50102000,
    /** 未知错误，请查看日志 */
    NATIVE_ERROR_UNKNOWN = 50002000,
    /** egl环境状态异常 */
    NATIVE_ERROR_EGL_STATE_UNKNOWN = 60001000,
    /** egl接口调用失败 */
    NATIVE_ERROR_EGL_API_FAILED = 60002000,
} OHNativeErrorCode;

/**
 * @brief OH_NativeWindow_NativeWindowHandleOpt函数中的操作码。
 * @since 8
 */
typedef enum NativeWindowOperation {
    /**
     * 设置本地窗口缓冲区几何图形，
     * 函数中的可变参数是
     * [输入] int32_t width，[输入] int32_t height。
     */
    SET_BUFFER_GEOMETRY,
    /**
     * 获取本地窗口缓冲区几何图形，
     * 函数中的可变参数是
     * [输出] int32_t *height， [输出] int32_t *width。
     */
    GET_BUFFER_GEOMETRY,
    /**
     * 获取本地窗口缓冲区格式，
     * 函数中的可变参数是
     * [输出] int32_t *format。
     */
    GET_FORMAT,
    /** 
     * 设置本地窗口缓冲区格式，
     * 函数中的可变参数是
     * [输入] int32_t format。
     */
    SET_FORMAT,
    /** 
     * 获取本地窗口读写方式，
     * 函数中的可变参数是
     * [输出] int32_t *usage。
     */
    GET_USAGE,
    /** 
     * 设置本地窗口缓冲区读写方式，
     * 函数中的可变参数是
     * [输入] int32_t usage。
     */
    SET_USAGE,
    /** 
     * 设置本地窗口缓冲区步幅，
     * 函数中的可变参数是
     * [输入] int32_t stride。
     */
    SET_STRIDE,
    /** 
     * 获取本地窗口缓冲区步幅，
     * 函数中的可变参数是
     * [输出] int32_t *stride。
     */
    GET_STRIDE,
    /** 
     * 设置本地窗口缓冲区交换间隔，
     * 函数中的可变参数是
     * [输入] int32_t interval。
     */
    SET_SWAP_INTERVAL,
    /** 
     * 获取本地窗口缓冲区交换间隔，
     * 函数中的可变参数是
     * [输出] int32_t *interval。
     */
    GET_SWAP_INTERVAL,
    /** 
     * 设置请求本地窗口缓冲区的超时等待时间，
     * 函数中的可变参数是
     * [输入] int32_t timeout。
     */
    SET_TIMEOUT,
    /** 
     * 获取请求本地窗口缓冲区的超时等待时间，
     * 函数中的可变参数是
     * [输出] int32_t *timeout。
     */
    GET_TIMEOUT,
    /** 
     * 设置本地窗口缓冲区色彩空间，
     * 函数中的可变参数是
     * [输入] int32_t colorGamut。
     */
    SET_COLOR_GAMUT,
    /** 
     * 获取本地窗口缓冲区色彩空间，
     * 函数中的可变参数是
     * [out int32_t *colorGamut]。
     */
    GET_COLOR_GAMUT,
    /** 
     * 设置本地窗口缓冲区变换，
     * 函数中的可变参数是
     * [输入] int32_t transform。
     */
    SET_TRANSFORM,
    /** 
     * 获取本地窗口缓冲区变换，
     * 函数中的可变参数是
     * [输出] int32_t *transform。
     */
    GET_TRANSFORM,
    /** 
     * 设置本地窗口缓冲区UI时间戳，
     * 函数中的可变参数是
     * [输入] uint64_t uiTimestamp。
     */
    SET_UI_TIMESTAMP,
    /**
     * 获取内存队列大小,
     * 函数中的可变参数是
     * [输出] int32_t *size.
     * @since 12
     */
    GET_BUFFERQUEUE_SIZE,
    /**
     * 设置HDR白点亮度,
     * 函数中的可变参数是
     * [输入] float brightness. 取值范围为[0.0f, 1.0f].
     * @since 12
     */
    SET_HDR_WHITE_POINT_BRIGHTNESS,
    /**
     * 设置SDR白点亮度,
     * 函数中的可变参数是
     * [输入] float brightness. 取值范围为[0.0f, 1.0f].
     * @since 12
     */
    SET_SDR_WHITE_POINT_BRIGHTNESS,
} NativeWindowOperation;

/**
 * @brief 缩放模式 Scaling Mode。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef enum {
    /**
     * 在接收到窗口大小的缓冲区之前，不可以更新窗口内容。
     */
    OH_SCALING_MODE_FREEZE = 0,
    /**
     * 缓冲区在二维中缩放以匹配窗口大小。
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW,
    /**
     * 缓冲区被统一缩放，使得缓冲区的较小尺寸与窗口大小匹配。
     */
    OH_SCALING_MODE_SCALE_CROP,
    /**
     * 窗口被裁剪为缓冲区裁剪矩形的大小，裁剪矩形之外的像素被视为完全透明。
     */
    OH_SCALING_MODE_NO_SCALE_CROP,
} OHScalingMode;

/**
 * @brief 渲染缩放模式枚举。
 * @since 12
 */
typedef enum OHScalingModeV2 {
    /**
     * 冻结窗口，在接收到和窗口大小相等的缓冲区之前，窗口内容不进行更新。
     */
    OH_SCALING_MODE_FREEZE_V2 = 0,
    /**
     * 缓冲区进行拉伸缩放以匹配窗口大小。
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW_V2,
    /**
     * 缓冲区按原比例缩放，使得缓冲区的较小边与窗口匹配，
     * 较长边超出窗口部分被视为透明。
     */
    OH_SCALING_MODE_SCALE_CROP_V2,
    /**
     * 按窗口大小将缓冲区裁剪，裁剪矩形之外的像素被视为完全透明。
     */
    OH_SCALING_MODE_NO_SCALE_CROP_V2,
    /**
     * 缓冲区按原比例缩放。优先显示所有缓冲区内容。
     * 如果比例与窗口比例不同，用背景颜色填充窗口的未填充区域。
     * 开发板和模拟器不支持该模式。
     */
    OH_SCALING_MODE_SCALE_FIT_V2,
} OHScalingModeV2;

/**
 * @brief 枚举HDR元数据关键字。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef enum {
    OH_METAKEY_RED_PRIMARY_X = 0, /**< 红基色X坐标。 */
    OH_METAKEY_RED_PRIMARY_Y = 1, /**<  红基色Y坐标。*/
    OH_METAKEY_GREEN_PRIMARY_X = 2, /**<  绿基色X坐标。 */
    OH_METAKEY_GREEN_PRIMARY_Y = 3, /**<  绿基色Y坐标。 */
    OH_METAKEY_BLUE_PRIMARY_X = 4, /**<  蓝基色X坐标。 */
    OH_METAKEY_BLUE_PRIMARY_Y = 5, /**<  蓝基色Y坐标。 */
    OH_METAKEY_WHITE_PRIMARY_X = 6, /**<  白点X坐标。 */
    OH_METAKEY_WHITE_PRIMARY_Y = 7, /**<  白点Y坐标。 */
    OH_METAKEY_MAX_LUMINANCE = 8, /**<  最大的光亮度。 */
    OH_METAKEY_MIN_LUMINANCE = 9, /**<  最小的光亮度。 */
    OH_METAKEY_MAX_CONTENT_LIGHT_LEVEL = 10, /**<  最大的内容亮度水平。 */
    OH_METAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11, /**<  最大的帧平均亮度水平。 */
    OH_METAKEY_HDR10_PLUS = 12, /**<  HDR10 Plus。 */
    OH_METAKEY_HDR_VIVID = 13, /**<  Vivid。 */
} OHHDRMetadataKey;

/**
 * @brief HDR元数据结构体定义。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef struct {
    OHHDRMetadataKey key; /**<  HDR元数据关键字 */
    float value; /**<  关键字对应的值 */
} OHHDRMetaData;

/**
 * @brief 扩展数据句柄结构体定义。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef struct {
    int32_t fd; /**<  句柄 Fd， -1代表不支持 */
    uint32_t reserveInts; /**<  Reserve数组的个数 */
    int32_t reserve[0]; /**<  Reserve数组 */
} OHExtDataHandle;

/**
 * @brief 创建OHNativeWindow实例，每次调用都会产生一个新的OHNativeWindow实例。
 * 说明：此接口不可用，可通过<b>OH_NativeImage_AcquireNativeWindow</b>创建，或通过XComponent创建。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurface 一个指向生产者ProduceSurface的指针，类型为sptr<OHOS::Surface>。
 * @return 返回一个指针，指向OHNativeWindow的结构体实例。
 * @since 8
 * @version 1.0
 */
OHNativeWindow* OH_NativeWindow_CreateNativeWindow(void* pSurface);

/**
 * @brief 将OHNativeWindow对象的引用计数减1，当引用计数为0的时候，该OHNativeWindow对象会被析构掉。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindow(OHNativeWindow* window);

/**
 * @brief 创建OHNativeWindowBuffer实例，每次调用都会产生一个新的OHNativeWindowBuffer实例。
 * 说明：此接口不可用，使用<b>OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer</b>替代。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurfaceBuffer 一个指向生产者buffer的指针，类型为sptr<OHOS::SurfaceBuffer>。
 * @return 返回一个指针，指向OHNativeWindowBuffer的结构体实例。
 * @since 8
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromSurfaceBuffer(void* pSurfaceBuffer);

/**
 * @brief 创建OHNativeWindowBuffer实例，每次调用都会产生一个新的OHNativeWindowBuffer实例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param nativeBuffer 一个指向OH_NativeBuffer的指针。
 * @return 返回一个指针，指向OHNativeWindowBuffer的结构体实例。
 * @since 11
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer(OH_NativeBuffer* nativeBuffer);

/**
 * @brief 将OHNativeWindowBuffer对象的引用计数减1，当引用计数为0的时候，该OHNativeWindowBuffer对象会被析构掉。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindowBuffer(OHNativeWindowBuffer* buffer);

/**
 * @brief 通过OHNativeWindow对象申请一块OHNativeWindowBuffer，用以内容生产。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的二级指针。
 * @param fenceFd 一个文件描述符句柄。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowRequestBuffer(OHNativeWindow *window,
    OHNativeWindowBuffer **buffer, int *fenceFd);

/**
 * @brief 通过OHNativeWindow将生产好内容的OHNativeWindowBuffer放回到Buffer队列中，用以内容消费。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @param fenceFd 一个文件描述符句柄，用以同步时序。
 * @param region 表示一块脏区域，该区域有内容更新。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowFlushBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer,
    int fenceFd, Region region);

/**
 * @brief 从OHNativeWindow获取上次送回到buffer队列中的OHNativeWindowBuffer。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer结构体指针的指针。
 * @param fenceFd 一个文件描述符的指针。
 * @param matrix 表示检索到的4*4变换矩阵。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeWindow_GetLastFlushedBuffer(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief 通过OHNativeWindow将之前申请出来的OHNativeWindowBuffer返还到Buffer队列中，供下次再申请。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAbortBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 设置/获取OHNativeWindow的属性，包括设置/获取宽高、内容格式等。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param code 表示操作码，详见{@link NativeWindowOperation}。
 * @param ... 可变参数，必须与操作码一一对应。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowHandleOpt(OHNativeWindow *window, int code, ...);

/**
 * @brief 通过OHNativeWindowBuffer获取该buffer的BufferHandle指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return BufferHandle 返回一个指针，指向BufferHandle的结构体实例。
 * @since 8
 * @version 1.0
 */
BufferHandle *OH_NativeWindow_GetBufferHandleFromNative(OHNativeWindowBuffer *buffer);

/**
 * @brief 增加一个NativeObject的引用计数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectReference(void *obj);

/**
 * @brief 减少一个NativeObject的引用计数，当引用计数减少为0时，该NativeObject将被析构掉。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectUnreference(void *obj);

/**
 * @brief 获取NativeObject的MagicId。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return MagicId 返回值为魔鬼数字，每个NativeObject唯一。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_GetNativeObjectMagic(void *obj);

/**
 * @brief 设置OHNativeWindow的ScalingMode。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param scalingMode 枚举值OHScalingMode。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetScalingMode(OHNativeWindow *window, uint32_t sequence,
                                                   OHScalingMode scalingMode);

/**
 * @brief 设置OHNativeWindow的元数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param size OHHDRMetaData数组的大小。
 * @param metaDate 指向OHHDRMetaData数组的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetMetaData(OHNativeWindow *window, uint32_t sequence, int32_t size,
                                                const OHHDRMetaData *metaData);

/**
 * @brief 设置OHNativeWindow的元数据集。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param key 枚举值OHHDRMetadataKey。
 * @param size uint8_t向量的大小。
 * @param metaDate 指向uint8_t向量的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetMetaDataSet(OHNativeWindow *window, uint32_t sequence, OHHDRMetadataKey key,
                                                   int32_t size, const uint8_t *metaData);

/**
 * @brief 设置OHNativeWindow的TunnelHandle。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param handle 指向OHExtDataHandle的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetTunnelHandle(OHNativeWindow *window, const OHExtDataHandle *handle);

/**
 * @brief 将OHNativeWindowBuffer添加进OHNativeWindow中。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAttachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 将OHNativeWindowBuffer从OHNativeWindow中分离。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowDetachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 通过OHNativeWindow获取对应的surfaceId。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param surfaceId 一个surface对应ID的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetSurfaceId(OHNativeWindow *window, uint64_t *surfaceId);

/**
 * @brief 通过surfaceId创建对应的OHNativeWindow。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param surfaceId 一个surface对应的ID。
 * @param window 一个OHNativeWindow的结构体实例的二级指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_CreateNativeWindowFromSurfaceId(uint64_t surfaceId, OHNativeWindow **window);

/**
 * @brief 设置OHNativeWindow的渲染缩放模式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param scalingMode 一个OHScalingModeV2类型的枚举值。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetScalingModeV2(OHNativeWindow* window, OHScalingModeV2 scalingMode);

/**
 * @brief 从OHNativeWindow获取上次送回到buffer队列中的OHNativeWindowBuffer,
 * 与OH_NativeWindow_GetLastFlushedBuffer的差异在于matrix不同。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer结构体指针的指针。
 * @param fenceFd 一个文件描述符的指针。
 * @param matrix 表示检索到的4*4变换矩阵。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetLastFlushedBufferV2(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief 提前缓存一帧buffer，且缓存的这一帧延迟一帧上屏显示，以此抵消后续一次超长帧丢帧。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个{@link OHNativeWindow}的结构体实例的指针。
 * @since 12
 * @version 1.0
 */
void OH_NativeWindow_SetBufferHold(OHNativeWindow *window);

/**
 * @brief 将窗口对象写入IPC序列化对象中。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param parcel 一个指向{@link OHIPCParcel}的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_WriteToParcel(OHNativeWindow *window, OHIPCParcel *parcel);

/**
 * @brief 从IPC序列化对象中读取窗口对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param parcel 一个指向{@link OHIPCParcel}的结构体实例的指针。
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的二级指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_ReadFromParcel(OHIPCParcel *parcel, OHNativeWindow **window);

#ifdef __cplusplus
}
#endif

/** @} */
#endif