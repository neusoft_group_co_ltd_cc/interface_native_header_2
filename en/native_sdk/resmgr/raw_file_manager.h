/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup rawfile
 * @{
 *
 * @brief Provides the function of operating rawfile directories and rawfiles.
 *
 * These functions include traversing, opening, searching, reading, and closing rawfiles.
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file resource_manager.h
 *
 * @brief Provides functions for managing rawfile resources.
 *
 * You can use the resource manager to open a rawfile and perform operations such as data search and reading.
 *
 * @since 8
 * @version 1.0
 */
#ifndef GLOBAL_NATIVE_RESOURCE_MANAGER_H
#define GLOBAL_NATIVE_RESOURCE_MANAGER_H

#include "napi/native_api.h"
#include "raw_dir.h"
#include "raw_file.h"

#ifdef __cplusplus
extern "C" {
#endif

struct NativeResourceManager;

/**
 * @brief Implements the resource manager.
 *
 * This class encapsulates the native implementation of the JavaScript resource manager.
 * You can obtain the pointer to <b>ResourceManager</b> by calling {@link OH_ResourceManager_InitNativeResourceManager}.
 *
 * @since 8
 * @version 1.0
 */
typedef struct NativeResourceManager NativeResourceManager;

/**
 * @brief Obtains the native resource manager based on JavaScipt resource manager.
 *
 * After obtaining a resource manager, you can use it complete various rawfile operations.
 *
 * @param env Indicates the pointer to the JavaScipt Native Interface (napi) environment.
 * @param jsResMgr Indicates the JavaScipt resource manager.
 * @return Returns the pointer to {@link NativeResourceManager}.
 * @since 8
 * @version 1.0
 */
NativeResourceManager *OH_ResourceManager_InitNativeResourceManager(napi_env env, napi_value jsResMgr);

/**
 * @brief Releases a native resource manager.
 *
 *
 *
 * @param resMgr Indicates the pointer to {@link NativeResourceManager}.
 * @since 8
 * @version 1.0
 */
void OH_ResourceManager_ReleaseNativeResourceManager(NativeResourceManager *resMgr);

/**
 * @brief Opens a rawfile directory.
 *
 * After opening a rawfile directory, you can traverse all the rawfile files in it.
 *
 * @param mgr Indicates the pointer to {@link NativeResourceManager}. You can obtain this pointer by
 * calling {@link OH_ResourceManager_InitNativeResourceManager}.
 * @param dirName Indicates the name of the rawfile directory to open. If this field is left empty, 
 * the root directory of rawfile will be opened.
 * @return Returns the pointer to {@link RawDir}. If this pointer is no longer needed after use, 
 * call {@link OH_ResourceManager_CloseRawDir} to release it.
 * @see OH_ResourceManager_InitNativeResourceManager
 * @see OH_ResourceManager_CloseRawDir
 * @since 8
 * @version 1.0
 */
RawDir *OH_ResourceManager_OpenRawDir(const NativeResourceManager *mgr, const char *dirName);

/**
 * @brief Opens a rawfile.
 *
 * After a rawfile is opened, you can read the data in it.
 *
 * @param mgr Indicates the pointer to {@link NativeResourceManager}. You can obtain this pointer by
 * calling {@link OH_ResourceManager_InitNativeResourceManager}.
 * @param fileName Indicates the file name in the relative path of the rawfile root directory.
 * @return Returns the pointer to {@link RawFile}. If this pointer is no longer needed after use, 
 * call {@link OH_ResourceManager_CloseRawFile} to release it.
 * @see OH_ResourceManager_InitNativeResourceManager
 * @see OH_ResourceManager_CloseRawFile
 * @since 8
 * @version 1.0
 */
RawFile *OH_ResourceManager_OpenRawFile(const NativeResourceManager *mgr, const char *fileName);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // GLOBAL_NATIVE_RESOURCE_MANAGER_H
