/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_
#define NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_

/**
 * @addtogroup NativeWindow
 * @{
 *
 * @brief Provides the <b>NativeWindow</b> capability for connection to the EGL.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @since 8
 * @version 1.0
 */

/**
 * @file external_window.h
 *
 * @brief Defines the functions for obtaining and using <b>NativeWindow</b>.
 *
 * File to include: <native_window/external_window.h>
 * @library libnative_window.so
 * @since 8
 * @version 1.0
 */

#include <stdint.h>
#include "buffer_handle.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Defines the <b>NativeWindow</b> struct.
 * @since 8
 */
struct NativeWindow;

/**
 * @brief Defines the <b>NativeWindowBuffer</b> struct.
 * @since 8
 */
struct NativeWindowBuffer;

/**
 * @brief Provides access to <b>OHNativeWindow</b>.
 * @since 8
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief Provides access to <b>OHNativeWindowBuffer</b>.
 * @since 8
 */
typedef struct NativeWindowBuffer OHNativeWindowBuffer;

/**
 * @brief Defines the rectangle (dirty region) where the content is to be updated in the local <b>OHNativeWindow</b>.
 * @since 8
 */
typedef struct Region {
    /** If <b>rects</b> is a null pointer, the buffer size is the same as the size of the dirty region by default. */
    struct Rect {
        int32_t x; /**< Start X coordinate of the rectangle. */
        int32_t y; /**< Start Y coordinate of the rectangle. */
        uint32_t w; /**< Width of the rectangle. */
        uint32_t h; /**< Height of the rectangle. */
    } *rects;
    /** If <b>rectNumber</b> is <b>0</b>, the buffer size is the same as the size of the dirty region by default. */
    int32_t rectNumber;
}Region;

/**
 * @brief Enumerates the error codes.
 * @since 12
 */
typedef enum OHNativeErrorCode {
    /** The operation is successful. */
    NATIVE_ERROR_OK = 0,
    /** An input parameter is invalid. */
    NATIVE_ERROR_INVALID_ARGUMENTS = 40001000,
    /** You do not have the permission to perform the operation. */
    NATIVE_ERROR_NO_PERMISSION = 40301000,
    /** No buffer is available. */
    NATIVE_ERROR_NO_BUFFER = 40601000,
    /** The consumer does not exist. */
    NATIVE_ERROR_NO_CONSUMER = 41202000,
    /** Not initialized. */
    NATIVE_ERROR_NOT_INIT = 41203000,
    /** The consumer is connected. */
    NATIVE_ERROR_CONSUMER_IS_CONNECTED = 41206000,
    /** The buffer status does not meet the expectation. */
    NATIVE_ERROR_BUFFER_STATE_INVALID = 41207000,
    /** The buffer is already in the buffer queue. */
    NATIVE_ERROR_BUFFER_IS_INCACHE = 41208000,
    /** The queue is full. */
    NATIVE_ERROR_BUFFER_QUEUE_FULL = 41209000,
    /** The buffer is not in the buffer queue. */
    NATIVE_ERROR_BUFFER_NOT_INCACHE = 41210000,
    /** The device or platform does not support the operation. */
    NATIVE_ERROR_UNSUPPORT = 50102000,
    /** Unknown error. Check the log. */
    NATIVE_ERROR_UNKNOW = 50002000,
    /** The EGL environment is abnormal. */
    NATIVE_ERROR_EGL_STATE_UNKNOW = 60001000,
    /** Failed to call the EGL APIs. */
    NATIVE_ERROR_EGL_API_FAILED = 60002000,
} OHNativeErrorCode;

/**
 * @brief Enumerates the operation codes in the <b>OH_NativeWindow_NativeWindowHandleOpt</b> function.
 * @since 8
 */
typedef enum NativeWindowOperation {
    /**
     * Setting the geometry for the local window buffer.
     * Variable arguments in the function:
     * [Input] int32_t width and [Input] int32_t height.
     */
    SET_BUFFER_GEOMETRY,
    /**
     * Obtaining the geometry of the local window buffer.
     * Variable arguments in the function:
     * [Output] int32_t *height and [Output] int32_t *width.
     */
    GET_BUFFER_GEOMETRY,
    /**
     * Obtaining the format of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *format.
     */
    GET_FORMAT,
    /** 
     * Setting the format for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t format.
     */
    SET_FORMAT,
    /** 
     * Obtaining the read/write mode of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *usage.
     */
    GET_USAGE,
    /** 
     * Setting the read/write mode for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t usage.
     */
    SET_USAGE,
    /** 
     * Setting the stride for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t stride.
     */
    SET_STRIDE,
    /** 
     * Obtaining the stride of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *stride.
     */
    GET_STRIDE,
    /** 
     * Setting the swap interval for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t interval.
     */
    SET_SWAP_INTERVAL,
    /** 
     * Obtaining the swap interval of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *interval.
     */
    GET_SWAP_INTERVAL,
    /** 
     * Setting the timeout duration for requesting the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t timeout.
     */
    SET_TIMEOUT,
    /** 
     * Obtaining the timeout duration for requesting the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *timeout.
     */
    GET_TIMEOUT,
    /** 
     * Setting the color gamut for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t colorGamut.
     */
    SET_COLOR_GAMUT,
    /** 
     * Obtaining the color gamut of the local window buffer.
     * Variable argument in the function:
     * [out int32_t *colorGamut].
     */
    GET_COLOR_GAMUT,
    /** 
     * Setting the transform for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t transform.
     */
    SET_TRANSFORM,
    /** 
     * Obtaining the transform of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *transform.
     */
    GET_TRANSFORM,
    /** 
     * Setting the UI timestamp for the local window buffer.
     * Variable argument in the function:
     * [Input] uint64_t uiTimestamp.
     */
    SET_UI_TIMESTAMP,
    /**
     * Obtaining the memory queue size,
     * Variable arguments in the function:
     * [Output] int32_t *size.
     * @since 12
     */
    GET_BUFFERQUEUE_SIZE,
} NativeWindowOperation;

/**
 * @brief Enumerates the scaling modes.
 * @since 9
 * @deprecated This enum is deprecated since API version 10. No substitute enum is provided.
 */
typedef enum {
    /**
     * The window content cannot be updated before the buffer of the window size is received.
     */
    OH_SCALING_MODE_FREEZE = 0,
    /**
     * The buffer is scaled in two dimensions to match the window size.
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW,
    /**
     * The buffer is scaled uniformly so that its smaller size can match the window size.
     */
    OH_SCALING_MODE_SCALE_CROP,
    /**
     * The window is cropped to the size of the buffer's cropping rectangle.
     * Pixels outside the cropping rectangle are considered completely transparent.
     */
    OH_SCALING_MODE_NO_SCALE_CROP,
} OHScalingMode;

/**
 * @brief Enumerates the rendering scaling modes.
 * @since 12
 */
typedef enum OHScalingModeV2 {
    /**
     * Freezes the window. The window content is not updated until a buffer with the same size as the window
     * is received.
     */
    OH_SCALING_MODE_FREEZE_V2 = 0,
    /**
     * Scales the buffer to match the window size.
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW_V2,
    /**
     * Scales the buffer at the original aspect ratio to enable the smaller side of the buffer to match the window,
     * while making the excess part transparent.
     */
    OH_SCALING_MODE_SCALE_CROP_V2,
    /**
     * Crops the buffer by window size. Pixels outside the cropping rectangle are considered completely transparent.
     */
    OH_SCALING_MODE_NO_SCALE_CROP_V2,
    /**
     * Scales the buffer at the original aspect ratio to fully display the buffer content,
     * while filling the unfilled area of the window with the background color.
     * This mode is not available for the development board and emulator.
     */
    OH_SCALING_MODE_SCALE_FIT_V2,
} OHScalingModeV2;

/**
 * @brief Enumerates the HDR metadata keys.
 * @since 9
 * @deprecated This enum is deprecated since API version 10. No substitute enum is provided.
 */
typedef enum {
    OH_METAKEY_RED_PRIMARY_X = 0, /**< X coordinate of the red primary color. */
    OH_METAKEY_RED_PRIMARY_Y = 1, /**< Y coordinate of the red primary color. */
    OH_METAKEY_GREEN_PRIMARY_X = 2, /**< X coordinate of the green primary color. */
    OH_METAKEY_GREEN_PRIMARY_Y = 3, /**< Y coordinate of the green primary color. */
    OH_METAKEY_BLUE_PRIMARY_X = 4, /**< X coordinate of the blue primary color. */
    OH_METAKEY_BLUE_PRIMARY_Y = 5, /**< Y coordinate of the blue primary color. */
    OH_METAKEY_WHITE_PRIMARY_X = 6, /**< X coordinate of the white point. */
    OH_METAKEY_WHITE_PRIMARY_Y = 7, /**< Y coordinate of the white point. */
    OH_METAKEY_MAX_LUMINANCE = 8, /**< Maximum luminance. */
    OH_METAKEY_MIN_LUMINANCE = 9, /**< Minimum luminance. */
    OH_METAKEY_MAX_CONTENT_LIGHT_LEVEL = 10, /**< Maximum content light level (MaxCLL). */
    OH_METAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11, /**< Maximum frame average light level (MaxFALL). */
    OH_METAKEY_HDR10_PLUS = 12, /**< HDR10+. */
    OH_METAKEY_HDR_VIVID = 13, /**< Vivid. */
} OHHDRMetadataKey;

/**
 * @brief Defines the HDR metadata.
 * @since 9
 * @deprecated This struct is deprecated since API version 10. No substitute struct is provided.
 */
typedef struct {
    OHHDRMetadataKey key; /**< Key of the HDR metadata. */
    float value; /**< Value corresponding to the metadata key. */
} OHHDRMetaData;

/**
 * @brief Defines the extended data handle.
 * @since 9
 * @deprecated This struct is deprecated since API version 10. No substitute struct is provided.
 */
typedef struct {
    int32_t fd; /**< File descriptor handle. The value <b>-1</b> means that the handle is not supported. */
    uint32_t reserveInts; /**< Number of reserved arrays. */
    int32_t reserve[0]; /**< Reserved array. */
} OHExtDataHandle;

/**
 * @brief Creates an <b>OHNativeWindow</b> instance.
 * A new <b>OHNativeWindow</b> instance is created each time this function is called.
 * If this API is unavailable, you can create an <b>OHNativeWindow</b> instance by calling
 * <b>OH_NativeImage_AcquireNativeWindow</b> or through the <b><XComponent></b>.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurface Pointer to a <b>ProduceSurface</b>. The type is <b>sptr<OHOS::Surface></b>.
 * @return Returns the pointer to the <b>OHNativeWindow</b> instance created.
 * @since 8
 * @version 1.0
 */
OHNativeWindow* OH_NativeWindow_CreateNativeWindow(void* pSurface);

/**
 * @brief Decreases the reference count of an <b>OHNativeWindow</b> instance by 1 and
 * when the reference count reaches 0, destroys the instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindow(OHNativeWindow* window);

/**
 * @brief Creates an <b>OHNativeWindowBuffer</b> instance.
 * A new <b>OHNativeWindowBuffer</b> instance is created each time this function is called.
 * If this API is unavailable, you can create an <b>OHNativeWindowBuffer</b> instance
 * by calling <b>OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer</b>.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurfaceBuffer Pointer to a produce buffer. The type is <b>sptr<OHOS::SurfaceBuffer></b>.
 * @return Returns the pointer to the <b>OHNativeWindowBuffer</b> instance created.
 * @since 8
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromSurfaceBuffer(void* pSurfaceBuffer);

/**
 * @brief Creates an <b>OHNativeWindowBuffer</b> instance.
 * A new <b>OHNativeWindowBuffer</b> instance is created each time this function is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param nativeBuffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns the pointer to the <b>OHNativeWindowBuffer</b> instance created.
 * @since 11
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer(OH_NativeBuffer* nativeBuffer);

/**
 * @brief Decreases the reference count of an <b>OHNativeWindowBuffer</b> instance by 1
 * and when the reference count reaches 0, destroys the instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindowBuffer(OHNativeWindowBuffer* buffer);

/**
 * @brief Requests an <b>OHNativeWindowBuffer</b> through an <b>OHNativeWindow</b> instance for content production.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Double pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @param fenceFd Pointer to a file descriptor handle.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowRequestBuffer(OHNativeWindow *window,
    OHNativeWindowBuffer **buffer, int *fenceFd);

/**
 * @brief Flushes the <b>OHNativeWindowBuffer</b> filled with the content to the buffer queue
 * through an <b>OHNativeWindow</b> instance for content consumption.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @param fenceFd File descriptor handle, which is used for timing synchronization.
 * @param region Dirty region where content is updated.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowFlushBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer,
    int fenceFd, Region region);

/**
 * @brief Obtains the <b>OHNativeWindowBuffer</b> that was flushed to the buffer queue last time
 * through an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Double pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @param fenceFd Pointer to a file descriptor handle.
 * @param matrix Retrieved 4*4 transformation matrix.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeWindow_GetLastFlushedBuffer(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief Returns the <b>OHNativeWindowBuffer</b> to the buffer queue through an <b>OHNativeWindow</b> instance,
 * without filling in any content. The <b>OHNativeWindowBuffer</b> can be used for a new request.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAbortBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief Sets or obtains the attributes of an <b>OHNativeWindow</b> instance,
 * including the width, height, and content format.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param code Operation code. For details, see {@link NativeWindowOperation}.
 * @param ... Variable argument, which must correspond to the operation code.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowHandleOpt(OHNativeWindow *window, int code, ...);

/**
 * @brief Obtains the pointer to a <b>BufferHandle</b> of an <b>OHNativeWindowBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @return Returns the pointer to the <b>BufferHandle</b> instance obtained.
 * @since 8
 * @version 1.0
 */
BufferHandle *OH_NativeWindow_GetBufferHandleFromNative(OHNativeWindowBuffer *buffer);

/**
 * @brief Adds the reference count of a native object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Pointer to an <b>OHNativeWindow</b> or <b>OHNativeWindowBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectReference(void *obj);

/**
 * @brief Decreases the reference count of a native object and when the reference count reaches 0, destroys this object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Pointer to an <b>OHNativeWindow</b> or <b>OHNativeWindowBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectUnreference(void *obj);

/**
 * @brief Obtains the magic ID of a native object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Pointer to an <b>OHNativeWindow</b> or <b>OHNativeWindowBuffer</b> instance.
 * @return Returns the magic ID, which is unique for each native object.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_GetNativeObjectMagic(void *obj);

/**
 * @brief Sets the scaling mode for an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param sequence Sequence of the producer buffer.
 * @param scalingMode Scaling mode to set. For details, see <b>OHScalingMode</b>.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 * @deprecated This API is deprecated since API version 10. No substitute API is provided.
 */
int32_t OH_NativeWindow_NativeWindowSetScalingMode(OHNativeWindow *window, uint32_t sequence,
                                                   OHScalingMode scalingMode);

/**
 * @brief Sets the metadata for an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param sequence Sequence of the producer buffer.
 * @param size Size of the <b>OHHDRMetaData</b> array.
 * @param metaData Pointer to the <b>OHHDRMetaData</b> array.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 * @deprecated This API is deprecated since API version 10. No substitute API is provided.
 */
int32_t OH_NativeWindow_NativeWindowSetMetaData(OHNativeWindow *window, uint32_t sequence, int32_t size,
                                                const OHHDRMetaData *metaData);

/**
 * @brief Sets the metadata set for an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param sequence Sequence of the producer buffer.
 * @param key Metadata key. For details, see <b>OHHDRMetadataKey</b>.
 * @param size Size of the uint8_t vector.
 * @param metaData Pointer to the uint8_t vector.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 * @deprecated This API is deprecated since API version 10. No substitute API is provided.
 */
int32_t OH_NativeWindow_NativeWindowSetMetaDataSet(OHNativeWindow *window, uint32_t sequence, OHHDRMetadataKey key,
                                                   int32_t size, const uint8_t *metaData);

/**
 * @brief Sets a tunnel handle for an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param handle Pointer to an <b>OHExtDataHandle</b>.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 * @deprecated This API is deprecated since API version 10. No substitute API is provided.
 */
int32_t OH_NativeWindow_NativeWindowSetTunnelHandle(OHNativeWindow *window, const OHExtDataHandle *handle);

/**
 * @brief Attaches an <b>OHNativeWindowBuffer</b> to an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAttachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief Detaches an <b>OHNativeWindowBuffer</b> from an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowDetachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief Obtains a surface ID through an <b>OHNativeWindow</b>.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param surfaceId Pointer to the surface ID.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetSurfaceId(OHNativeWindow *window, uint64_t *surfaceId);

/**
 * @brief Creates an <b>OHNativeWindow</b> instance based on a surface ID.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param surfaceId Pointer to the surface ID.
 * @param window Double pointer to an <b>OHNativeWindow</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_CreateNativeWindowFromSurfaceId(uint64_t surfaceId, OHNativeWindow **window);

/**
 * @brief Sets a rendering scaling mode for an <b>OHNativeWindow</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param scalingMode Scaling mode. For details about the available options, see <b>OHScalingModeV2</b>.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetScalingModeV2(OHNativeWindow* window, OHScalingModeV2 scalingMode);

/**
 * @brief Obtains the <b>OHNativeWindowBuffer</b> that was flushed to the buffer queue last time through
 * an <b>OHNativeWindow</b> instance.
 * The difference between this function and <b>OH_NativeWindow_GetLastFlushedBuffer</b> lies in the matrix.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an <b>OHNativeWindow</b> instance.
 * @param buffer Double pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @param fenceFd Pointer to a file descriptor handle.
 * @param matrix Retrieved 4*4 transformation matrix.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetLastFlushedBufferV2(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief Buffers a frame in advance and holds it for the interval of a frame to offset the possible loss of
 * subsequent oversized frames.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Pointer to an {@link OHNativeWindow} instance.
 * @since 12
 * @version 1.0
 */
void OH_NativeWindow_SetBufferHold(OHNativeWindow *window);
#ifdef __cplusplus
}
#endif

/** @} */
#endif
