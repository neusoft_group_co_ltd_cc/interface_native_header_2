/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_H
#define C_INCLUDE_DRAWING_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font.h
 *
 * @brief Declares the functions related to the font in the drawing module.
 *
 * File to include: native_drawing/drawing_font.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the font edging types.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontEdging {
    /** No anti-aliasing processing is used. */
    FONT_EDGING_ALIAS,
    /** Uses anti-aliasing to smooth the jagged edges. */
    FONT_EDGING_ANTI_ALIAS,
    /** Uses sub-pixel anti-aliasing to provide a smoother effect for jagged edges. */
    FONT_EDGING_SUBPIXEL_ANTI_ALIAS,
} OH_Drawing_FontEdging;

/**
 * @brief Enumerates the font hinting types.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontHinting {
    /** No font hinting is used. */
    FONT_HINTING_NONE,
    /** Slight font hinting is used to improve contrast. */
    FONT_HINTING_SLIGHT,
    /** Normal font hinting is used to improve contrast. */
    FONT_HINTING_NORMAL,
    /** Full font hinting is used to improve contrast. */
    FONT_HINTING_FULL,
} OH_Drawing_FontHinting;

/**
 * @brief Sets whether to request that baselines be snapped to pixels when the current canvas matrix is axis aligned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param baselineSnap Whether to request that baselines be snapped to pixels.
 * The value <b>true</b> means to request that baselines be snapped to pixels, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetBaselineSnap(OH_Drawing_Font*, bool baselineSnap);

/**
 * @brief Checks whether baselines are requested to be snapped to pixels when the current canvas matrix is axis aligned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if baselines are requested to be snapped to pixels when the current canvas matrix is
 * axis aligned; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsBaselineSnap(const OH_Drawing_Font*);

/**
 * @brief Sets a font edging effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_FontEdging Font edging effect.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEdging(OH_Drawing_Font*, OH_Drawing_FontEdging);

/**
 * @brief Obtains the font edging effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns the font edging effect.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontEdging OH_Drawing_FontGetEdging(const OH_Drawing_Font*);

/**
 * @brief Sets whether to forcibly use auto hinting, that is, whether to always hint glyphs.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param isForceAutoHinting Whether to forcibly use auto hinting.
 * The value <b>true</b> means to forcibly use auto hinting, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetForceAutoHinting(OH_Drawing_Font*, bool isForceAutoHinting);

/**
 * @brief Checks whether auto hinting is forcibly used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if auto hinting is forcibly used; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsForceAutoHinting(const OH_Drawing_Font*);

/**
 * @brief Sets whether to use sub-pixel rendering for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param isSubpixel Whether to use sub-pixel rendering for the font.
 * The value <b>true</b> means to use sub-pixel rendering for the font, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetSubpixel(OH_Drawing_Font*, bool isSubpixel);

/**
 * @brief Checks whether sub-pixel rendering is used for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if sub-pixel rendering is used for the font; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsSubpixel(const OH_Drawing_Font*);

/**
 * @brief Creates an <b>OH_Drawing_Font</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Font</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Font* OH_Drawing_FontCreate(void);

/**
 * @brief Sets the typeface for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param OH_Drawing_Typeface Pointer to an <b>OH_Drawing_Typeface</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTypeface(OH_Drawing_Font*, OH_Drawing_Typeface*);

/**
 * @brief Obtains the typeface of a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_FontGetTypeface(OH_Drawing_Font*);

/**
 * @brief Sets the font size.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param textSize Font size.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSize(OH_Drawing_Font*, float textSize);

/**
 * @brief Obtains the text size.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns a floating point number representing the text size.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSize(const OH_Drawing_Font*);

/**
 * @brief Obtains the number of glyphs represented by a string of text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param text Pointer to the start address for storing the text.
 * @param byteLength Text length, in bytes.
 * @param encoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @since 12
 * @version 1.0
 */
int OH_Drawing_FontCountText(OH_Drawing_Font*, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding);

/**
 * @brief Converts a string of text into glyph indices.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param text Pointer to the start address for storing the text.
 * @param byteLength Text length, in bytes.
 * @param encoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @param glyphs Pointer to the start address for storing the glyph indices.
 * @param maxGlyphCount Maximum number of glyphs.
 * @return Returns the number of glyph indices.
 * @since 12
 * @version 1.0
 */
uint32_t OH_Drawing_FontTextToGlyphs(const OH_Drawing_Font*, const void* text, uint32_t byteLength,
    OH_Drawing_TextEncoding encoding, uint16_t* glyphs, int maxGlyphCount);

/**
 * @brief Obtains the width of each glyph in a string of text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param glyphs Pointer to the start address for storing the glyph indices.
 * @param count Number of glyph indices.
 * @param widths Pointer to the start address for storing the glyph widths.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontGetWidths(const OH_Drawing_Font*, const uint16_t* glyphs, int count, float* widths);

/**
 * @brief Sets linear scaling for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param isLinearText Whether to set linear scaling.
 * The value <b>true</b> means to set linear scaling, and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetLinearText(OH_Drawing_Font*, bool isLinearText);

/**
 * @brief Checks whether linear scaling is used for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if linear scaling is used; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsLinearText(const OH_Drawing_Font*);

/**
 * @brief Sets a horizontal skew factor for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param skewX Skew of the X axis relative to the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSkewX(OH_Drawing_Font*, float skewX);

/**
 * @brief Obtains the horizontal skew factor of a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns a floating point number representing the horizontal skew factor.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSkewX(const OH_Drawing_Font*);

/**
 * @brief Sets fake bold for a font by increasing the stroke width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param isFakeBoldText Whether to set fake bold.
 * The value <b>true</b> means to set fake bold, and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetFakeBoldText(OH_Drawing_Font*, bool isFakeBoldText);

/**
 * @brief Checks whether fake bold is used for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if fake bold is used; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsFakeBoldText(const OH_Drawing_Font*);

/**
 * @brief Sets a horizontal scale factor for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param scaleX Horizontal scale factor.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetScaleX(OH_Drawing_Font*, float scaleX);

/**
 * @brief Obtains the horizontal scale factor of a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns the horizontal scale factor.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetScaleX(const OH_Drawing_Font*);

/**
 * @brief Sets a font hinting effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_FontHinting Font hinting effect.
 * For details about the available options, see {@link OH_Drawing_FontHinting}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetHinting(OH_Drawing_Font*, OH_Drawing_FontHinting);

/**
 * @brief Obtains the font hinting effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns the font hinting effect. For details about the available options, see {@link OH_Drawing_FontHinting}.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontHinting OH_Drawing_FontGetHinting(const OH_Drawing_Font*);

/**
 * @brief Sets whether to use bitmaps in a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param isEmbeddedBitmaps Whether to use bitmaps in the font. The value <b>true</b> means to use bitmaps in the font,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEmbeddedBitmaps(OH_Drawing_Font*, bool isEmbeddedBitmaps);

/**
 * @brief Checks whether bitmaps are used in a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns <b>true</b> if bitmaps are used; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsEmbeddedBitmaps(const OH_Drawing_Font*);

/**
 * @brief Destroys an <b>OH_Drawing_Font</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontDestroy(OH_Drawing_Font*);

/**
 * @brief Defines a struct that describes the measurement information about a font.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_Font_Metrics {
    /** Measurement information that is valid. */
    uint32_t fFlags;
    /** Maximum distance from the baseline to the highest coordinate of a character. */
    float top;
    /** Recommended distance from the baseline to the highest coordinate of a character. */
    float ascent;
    /** Recommended distance from the baseline to the lowest coordinate of a character. */
    float descent;
    /** Maximum distance from the baseline to the lowest coordinate of a character. */
    float bottom;
    /** Interline spacing. */
    float leading;
    /** Average character width, or zero if unknown. */
    float avgCharWidth;
    /** Maximum character width, or zero if unknown. */
    float maxCharWidth;
    /**
     * Maximum distance to the leftmost of the font bounding box. Generally, the value is a negative value.
     * Variable fonts are not recommended.
     */
    float xMin;
    /**
     * Maximum distance to the rightmost of the font bounding box. Generally, the value is a negative value.
     * Variable fonts are not recommended.
     */
    float xMax;
    /** Height of a lowercase letter, or zero if unknown. Generally, the value is a negative value. */
    float xHeight;
    /** Height of an uppercase letter, or zero if unknown. Generally, the value is a negative value. */
    float capHeight;
    /** Thickness of the underline. */
    float underlineThickness;
    /**
     * Position of the underline, that is, vertical distance from the baseline to the top of the underline.
     * Generally, the value is a positive value.
     */
    float underlinePosition;
    /** Thickness of the strikethrough. */
    float strikeoutThickness;
    /**
     * Position of the strikethrough, that is, vertical distance from the baseline to the bottom of the strikethrough.
     * Generally, the value is a negative value.
     */
    float strikeoutPosition;
} OH_Drawing_Font_Metrics;

/**
 * @brief Obtains the measurement information about a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_Font_Metrics Pointer to an {@link OH_Drawing_Font_Metrics} object.
 * @return Returns a floating-point variable that indicates the recommended interline spacing.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetMetrics(OH_Drawing_Font*, OH_Drawing_Font_Metrics*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
