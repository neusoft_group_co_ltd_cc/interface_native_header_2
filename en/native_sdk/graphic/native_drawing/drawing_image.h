/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_IMAGE_H
#define C_INCLUDE_DRAWING_IMAGE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_image.h
 *
 * @brief Declares the functions related to the image in the drawing module.
 *
 * File to include: native_drawing/drawing_image.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an {@link OH_Drawing_Image} object that describes an array of two-dimensional pixels to draw.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_Image} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Image* OH_Drawing_ImageCreate(void);

/**
 * @brief Destroys an {@link OH_Drawing_Image} object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ImageDestroy(OH_Drawing_Image*);

/**
 * @brief Builds an image from a bitmap by sharing or copying bitmap pixels. If the bitmap is marked as immutable,
 * the pixel memory is shared, not copied.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param OH_Drawing_Bitmap Pointer to an {@link OH_Drawing_Bitmap} object.
 * @return Returns <b>true</b> if the image is built; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_ImageBuildFromBitmap(OH_Drawing_Image*, OH_Drawing_Bitmap*);

/**
 * @brief Obtains the image width, that is, the number of pixels in each line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @return Returns the width.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_ImageGetWidth(OH_Drawing_Image*);

/**
 * @brief Obtains the image height, that is, the number of pixel lines.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @return Returns the height.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_ImageGetHeight(OH_Drawing_Image*);

/**
 * @brief Obtains the image information.
 * After this function is called, the passed-in image information object is filled.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param OH_Drawing_Image_Info Pointer to an {@link OH_Drawing_Image_Info} object,
 * which is obtained by calling {@link OH_Drawing_Image_Info}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ImageGetImageInfo(OH_Drawing_Image*, OH_Drawing_Image_Info*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
