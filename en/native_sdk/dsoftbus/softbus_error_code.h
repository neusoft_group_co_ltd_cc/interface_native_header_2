/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  */

/**
 * @addtogroup SoftBus
 * @{
 *
 * @brief Provides secure, high-speed communications between devices.
 *
 * This module implements unified distributed communication management of nearby devices and provides link-independent device discovery
 * and transmission interfaces to support service publishing and data transmission.
 * @since 1.0
 * @version 1.0
  */

/**
 * @file softbus_error_code.h
 *
 * @brief Defines the error codes of DSoftBus modules.
 *
 *
 * 
 *
 *
 * @since 1.0
 * @version 1.0
  */
#ifndef SOFTBUS_ERROR_CODE_H
#define SOFTBUS_ERROR_CODE_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief Enumerates the DSoftBus modules.
 *
 * @since 1.0
 * @version 1.0
  */
enum SoftBusModule {
    SOFTBUS_MOD_COMMON = 0,     /**< Common module */
    SOFTBUS_MOD_PLUGIN,         /**< Plugin module */
    SOFTBUS_MOD_TRANS,          /**< Transfer module */
    SOFTBUS_MOD_AUTH,           /**< Authentication module */
    SOFTBUS_MOD_LNN,            /**< LNN module */
    SOFTBUS_MOD_CONNECT,        /**< Connection module*/
    SOFTBUS_MOD_DISCOVERY,      /**< Discovery module*/
};

/**
 * @brief Defines the macro function for initial error codes of DSoftBus modules.
 *
 * @since 1.0
 * @version 1.0
  */
#define SOFTBUS_ERRNO(module) ((0xF << 28) | ((1 << (module)) << 16))


/**
 * @brief Enumerates the error codes of DSoftBus.
 *
 * @since 1.0
 * @version 1.0
  */
enum SoftBusErrNo {
    SOFTBUS_COMMOM_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_COMMON),      /**< Initial error codes of the common module */
    SOFTBUS_TIMOUT,                                                   /**< Timeout. */
    SOFTBUS_INVALID_PARAM,                                            /**< Invalid parameter. */
    SOFTBUS_MEM_ERR,                                                  /**< Memory error. */
    SOFTBUS_NOT_IMPLEMENT,                                            /**< Not implemented. */
    SOFTBUS_NO_URI_QUERY_KEY,                                         /**< No URI query key. */
    SOFTBUS_NO_INIT,                                                  /**< Not initialized. */
    SOFTBUS_PARSE_JSON_ERR,                                           /**< JSON parsing error. */
    SOFTBUS_PERMISSION_DENIED,                                        /**< No permission. */
    SOFTBUS_ACCESS_TOKEN_DENIED,                                      /**< Access token denied. */
    SOFTBUS_MALLOC_ERR,                                               /**< Memory allocation failed. */
    SOFTBUS_ENCRYPT_ERR,                                              /**< Encryption failed. */
    SOFTBUS_DECRYPT_ERR,                                              /**< Decryption failed. */
    SOFTBUS_INVALID_SESS_OPCODE,                                      /**< Invalid SESS_OPCODE. */
    SOFTBUS_INVALID_NUM,                                              /**< Invalid number. */
    SOFTBUS_SERVER_NAME_REPEATED,                                     /**< Duplicate server name. */
    SOFTBUS_TCP_SOCKET_ERR,                                           /**< TCP socket error. */
    SOFTBUS_LOCK_ERR,                                                 /**< Lock error. */
    SOFTBUS_GET_REMOTE_UUID_ERR,                                      /**< Failed to obtain the peer UUID. */
    SOFTBUS_NO_ENOUGH_DATA,                                           /**< Insufficient data length. */
    SOFTBUS_INVALID_DATA_HEAD,                                        /**< Invalid data header. */
    SOFTBUS_INVALID_FD,                                               /**< Invalid file descriptor (FD). */
    SOFTBUS_FILE_ERR,                                                 /**< File error. */
    SOFTBUS_DATA_NOT_ENOUGH,                                          /**< Lack of data. */
    SOFTBUS_SLICE_ERROR,                                              /**< Slice error. */
    SOFTBUS_ALREADY_EXISTED,                                          /**< Data already exists. */
    SOFTBUS_GET_CONFIG_VAL_ERR,                                       /**< Failed to obtain the configuration. */
    SOFTBUS_PEER_PROC_ERR,                                            /**< Peer processing error. */
    SOFTBUS_NOT_FIND,                                                 /**< Failed to find the target. */
    SOFTBUS_ALREADY_TRIGGERED,                                        /**< Already triggered. */
    SOFTBUS_FILE_BUSY,                                                /**< File unavailable. */
    SOFTBUS_IPC_ERR,                                                  /**< IPC error. */

    SOFTBUS_INVALID_PKGNAME,                                          /**< Invalid bundle name. */
    SOFTBUS_FUNC_NOT_SUPPORT,                                         /**< Not supported by the function. */
    SOFTBUS_SERVER_NOT_INIT,                                          /**< The server is not initialized. */

    SOFTBUS_PLUGIN_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_PLUGIN),      /**< Initial error codes of the plugin module. */

    SOFTBUS_TRANS_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_TRANS),        /**< Initial error codes of the transfer module. */
    SOFTBUS_TRANS_INVALID_SESSION_ID,                                 /**< Invalid session ID. */
    SOFTBUS_TRANS_INVALID_SESSION_NAME,                               /**< Invalid session name. */
    SOFTBUS_TRANS_INVALID_CHANNEL_TYPE,                               /**< Invalid channel type. */
    SOFTBUS_TRANS_INVALID_CLOSE_CHANNEL_ID,                           /**< The ID of the channel to close is invalid. */
    SOFTBUS_TRANS_SESSION_REPEATED,                                   /**< Duplicate session. */
    SOFTBUS_TRANS_SESSION_CNT_EXCEEDS_LIMIT,                          /**< The number of sessions exceeds the limit. */
    SOFTBUS_TRANS_SESSIONSERVER_NOT_CREATED,                          /**< The session server is not created. */
    SOFTBUS_TRANS_SESSION_OPENING,                                    /**< The session is being opened. */
    SOFTBUS_TRANS_GET_LANE_INFO_ERR,                                  /**< Failed to obtain lane information. */
    SOFTBUS_TRANS_CREATE_CHANNEL_ERR,                                 /**< Failed to create the channel. */
    SOFTBUS_TRANS_INVALID_DATA_LENGTH,                                /**< Invalid data length. */
    SOFTBUS_TRANS_FUNC_NOT_SUPPORT,                                   /**< Not supported by the function. */
    SOFTBUS_TRANS_OPEN_AUTH_CHANNEL_FAILED,                         /**< Failed to open the authentication channel. */
    SOFTBUS_TRANS_GET_P2P_INFO_FAILED,                                /**< Failed to obtain P2P information. */
    SOFTBUS_TRANS_OPEN_AUTH_CONN_FAILED,                              /**< Failed to set up the authentication connection. */

    SOFTBUS_TRANS_PROXY_PACKMSG_ERR,                                  /**< The proxy channel failed to pack the message. */
    SOFTBUS_TRANS_PROXY_SENDMSG_ERR,                                  /**< The proxy channel failed to send the message. */
    SOFTBUS_TRANS_PROXY_SEND_CHANNELID_INVALID,                       /**< Invalid proxy RX channel ID. */
    SOFTBUS_TRANS_PROXY_CHANNEL_STATUS_INVALID,                       /**< Invalid proxy channel status. */
    SOFTBUS_TRANS_PROXY_DEL_CHANNELID_INVALID,                        /**< The ID of the proxy channel to delete is invalid. */
    SOFTBUS_TRANS_PROXY_SESS_ENCRYPT_ERR,                             /**< The proxy channel failed to encrypt the session. */
    SOFTBUS_TRANS_PROXY_INVALID_SLICE_HEAD,                           /**< Invalid proxy slice header. */
    SOFTBUS_TRANS_PROXY_ASSEMBLE_PACK_NO_INVALID,                     /**< Invalid slice received. */
    SOFTBUS_TRANS_PROXY_ASSEMBLE_PACK_EXCEED_LENGTH,                  /**< The length of the slice received exceeds the limit. */
    SOFTBUS_TRANS_PROXY_ASSEMBLE_PACK_DATA_NULL,                      /**< The slice received is empty. */

    SOFTBUS_TRANS_UDP_CLOSE_CHANNELID_INVALID,                        /**< The ID of the UDP channel to close is invalid. */
    SOFTBUS_TRANS_UDP_SERVER_ADD_CHANNEL_FAILED,                      /**< Failed to add a channel to the UDP server. */
    SOFTBUS_TRANS_UDP_CLIENT_ADD_CHANNEL_FAILED,                      /**< Failed to add a channel to the UDP client. */
    SOFTBUS_TRANS_UDP_SERVER_NOTIFY_APP_OPEN_FAILED,                  /**< Failed to open the UDP channel on the UDP server. */
    SOFTBUS_TRANS_UDP_CLIENT_NOTIFY_APP_OPEN_FAILED,                  /**< Failed to open the UDP channel on the UDP client. */
    SOFTBUS_TRANS_UDP_START_STREAM_SERVER_FAILED,                     /**< Failed to start the UDP stream server. */
    SOFTBUS_TRANS_UDP_START_STREAM_CLIENT_FAILED,                     /**< Failed to start the UDP stream client. */
    SOFTBUS_TRANS_UDP_SEND_STREAM_FAILED,                             /**< Failed to send the UDP stream. */
    SOFTBUS_TRANS_UDP_GET_CHANNEL_FAILED,                             /**< Failed to obtain the UDP channel. */
    SOFTBUS_TRANS_UDP_CHANNEL_DISABLE,                                /**< Failed to disable the UDP channel. */

    SOFTBUS_TRANS_QOS_REPORT_FAILED,                                  /**< Failed to report the QoS. */
    SOFTBUS_TRANS_QOS_REPORT_TOO_FREQUENT,                            /**< The QoS is reported too frequently. */

    SOFTBUS_TRANS_SESSION_SERVER_NOINIT,                              /**< The session server is not initialized. */
    SOFTBUS_TRANS_SESSION_INFO_NOT_FOUND,                             /**< Failed to find session information. */
    SOFTBUS_TRANS_SESSION_CREATE_FAILED,                              /**< Failed to create the session. */
    SOFTBUS_TRANS_SESSION_ADDPKG_FAILED,                              /**< Failed to add the packet to the session. */
    SOFTBUS_TRANS_SESSION_SET_CHANNEL_FAILED,                         /**< Failed to set the session channel. */
    SOFTBUS_TRANS_SESSION_NO_ENABLE,                                  /**< The session is not enabled. */
    SOFTBUS_TRANS_SESSION_GROUP_INVALID,                              /**< Invalid session group. */
    SOFTBUS_TRANS_SESSION_NAME_NO_EXIST,                              /**< The session name does not exist. */
    SOFTBUS_TRANS_SESSION_GET_CHANNEL_FAILED,                         /**< Failed to obtain the session channel. */

    SOFTBUS_TRANS_PROXY_REMOTE_NULL,                                  /**< The remote proxy channel is null. */
    SOFTBUS_TRANS_PROXY_WRITETOKEN_FAILED,                            /**< Failed to write the token via the proxy channel. */
    SOFTBUS_TRANS_PROXY_WRITECSTRING_FAILED,                          /**< Failed to write the string via the proxy channel. */
    SOFTBUS_TRANS_PROXY_WRITERAWDATA_FAILED,                          /**< Failed to write raw data via the proxy channel. */
    SOFTBUS_TRANS_PROXY_READRAWDATA_FAILED,                           /**< Failed to read data via the proxy channel. */
    SOFTBUS_TRANS_PROXY_SEND_REQUEST_FAILED,                          /**< Failed to send the request via the proxy channel. */
    SOFTBUS_TRANS_PROXY_INVOKE_FAILED,                                /**< Failed to invoke the proxy channel. */
    SOFTBUS_TRANS_PROXY_CHANNEL_NOT_FOUND,                            /**< Failed to find the proxy channel. */

    SOFTBUS_TRANS_SEND_LEN_BEYOND_LIMIT,                              /**< The length of the data to send exceeds the limit. */
    SOFTBUS_TRANS_FILE_LISTENER_NOT_INIT,                             /**< The file listener is not initialized. */
    SOFTBUS_TRANS_STREAM_ONLY_UDP_CHANNEL,                            /**< Only UDP channels can be used to transfer streams. */
    SOFTBUS_TRANS_CHANNEL_TYPE_INVALID,                               /**< Invalid channel type. */
    SOFTBUS_TRANS_TDC_CHANNEL_NOT_FOUND,                              /**< Failed to find the TDC channel. */
    SOFTBUS_TRANS_TDC_CHANNEL_ALREADY_PENDING,                        /**< The TDC channel is suspended. */
    SOFTBUS_TRANS_TDC_PENDINGLIST_NOT_FOUND,                          /**< Failed to find the list of suspended TDC channels. */
    SOFTBUS_TRANS_AUTH_CHANNEL_NOT_FOUND,                             /**< Failed to find the authentication channel. */
    SOFTBUS_TRANS_NET_STATE_CHANGED,                                  /**< Network status is changed. */
    SOFTBUS_TRANS_HANDSHAKE_TIMEOUT,                                  /**< The transmission handshake timed out. */
    SOFTBUS_TRANS_HANDSHAKE_ERROR,                                    /**< Transmission handshake error. */
    SOFTBUS_TRANS_PEER_SESSION_NOT_CREATED,                           /**< The peer session is not created. */
    SOFTBUS_TRANS_PROXY_DISCONNECTED,                                 /**< The proxy channel is disconnected. */

    SOFTBUS_AUTH_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_AUTH),          /**< Initial error codes of the authentication module. */

    SOFTBUS_AUTH_INIT_FAIL,                                           /**< Authentication initialization failed. */
    SOFTBUS_AUTH_CONN_FAIL,                                           /**< Authentication connection failed. */
    SOFTBUS_AUTH_CONN_TIMEOUT,                                        /**< Authentication connection timed out. */
    SOFTBUS_AUTH_DEVICE_DISCONNECTED,                                 /**< The authenticated device is disconnected. */
    SOFTBUS_AUTH_SYNC_DEVID_FAIL,                                     /**< Failed to synchronize the authenticated device ID. */ 
    SOFTBUS_AUTH_UNPACK_DEVID_FAIL,                                   /**< Failed to unpack the device ID in authentication. */
    SOFTBUS_AUTH_HICHAIN_AUTH_FAIL,                                   /**< HiChain authentication failed. */
    SOFTBUS_AUTH_HICHAIN_PROCESS_FAIL,                                /**< HiChain process failed. */
    SOFTBUS_AUTH_HICHAIN_TRANSMIT_FAIL,                               /**< HiChain transmission failed. */
    SOFTBUS_AUTH_HICHAIN_AUTH_ERROR,                                  /**< HiChain authentication error. */
    SOFTBUS_AUTH_HICHAIN_NOT_TRUSTED,                                 /**< HiChain is untrusted. */
    SOFTBUS_AUTH_SYNC_DEVINFO_FAIL,                                   /**< Failed to synchronize device information in authentication. */
    SOFTBUS_AUTH_UNPACK_DEVINFO_FAIL,                                 /**< Failed to unpack the device information in authentication. */
    SOFTBUS_AUTH_SEND_FAIL,                                           /**< Failed to send authentication data. */
    SOFTBUS_AUTH_TIMEOUT,                                            /**< Authentication timed out. */
    SOFTBUS_AUTH_NOT_FOUND,                                           /**< Failed to find the AuthManager. */
    SOFTBUS_AUTH_INNER_ERR,                                           /**< Authentication internal error. */

    SOFTBUS_NETWORK_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_LNN),        /**< Initial error codes of the LNN module. */
    SOFTBUS_NETWORK_CONN_FSM_DEAD,                                    /**< The network connection FSM is dead. */
    SOFTBUS_NETWORK_JOIN_CANCELED,                                    /**< The request for joining the LNN is canceled. */
    SOFTBUS_NETWORK_JOIN_LEAVING,                                     /**< The device that requests to join the LNN is leaving. */
    SOFTBUS_NETWORK_JOIN_TIMEOUT,                                     /**< The join operation timed out. */
    SOFTBUS_NETWORK_UNPACK_DEV_INFO_FAILED,                           /**< Failed to unpack information about the device to join. */
    SOFTBUS_NETWORK_DEV_NOT_TRUST,                                    /**< The device to join the LNN is not trusted. */
    SOFTBUS_NETWORK_LEAVE_OFFLINE,                                    /**< The device is offline. */
    SOFTBUS_NETWORK_AUTH_DISCONNECT,                                  /**< The LNN authentication is disconnected. */
    SOFTBUS_NETWORK_TIME_SYNC_HANDSHAKE_ERR,                          /**< LNN time synchronization handshake error. */
    SOFTBUS_NETWORK_TIME_SYNC_HANDSHAKE_TIMEOUT,                      /**< The LNN time synchronization handshake timed out. */
    SOFTBUS_NETWORK_TIME_SYNC_TIMEOUT,                                /**< The LNN time synchronization timed out. */
    SOFTBUS_NETWORK_TIME_SYNC_INTERFERENCE,                           /**< The LNN time synchronization is interfered. */
    SOFTBUS_NETWORK_HEARTBEAT_REPEATED,                               /**< Duplicate LNN heartbeat. */
    SOFTBUS_NETWORK_HEARTBEAT_UNTRUSTED,                              /**< Untrusted LNN heartbeat. */
    SOFTBUS_NETWORK_HEARTBEAT_EMPTY_LIST,                             /**< The LNN heartbeat list is empty. */
    SOFTBUS_NETWORK_NODE_OFFLINE,                                     /**< The networking node is offline. */
    SOFTBUS_NETWORK_NOT_INIT,                                         /**< The LNN module is not initialized. */
    SOFTBUS_NETWORK_LOOPER_ERR,                                       /**< LNN looper error. */
    SOFTBUS_NETWORK_AUTH_TCP_ERR,                                     /**< LNN authentication TCP error. */
    SOFTBUS_NETWORK_AUTH_BLE_ERR,                                     /**< LNN authentication BLE error. */
    SOFTBUS_NETWORK_AUTH_BR_ERR,                                      /**< LNN authentication BR error. */
    SOFTBUS_NETWORK_GET_ALL_NODE_INFO_ERR,                            /**< Failed to obtain all node information in the LNN. */
    SOFTBUS_NETWORK_GET_LOCAL_NODE_INFO_ERR,                          /**< Failed to obtain local node information in the LNN. */
    SOFTBUS_NETWORK_NODE_KEY_INFO_ERR,                                /**< Incorrect key information of the LNN node. */
    SOFTBUS_NETWORK_ACTIVE_META_NODE_ERR,                             /**< Failed to activate the meta node in the LNN. */
    SOFTBUS_NETWORK_DEACTIVE_META_NODE_ERR,                           /**< Failed to deactivate the meta node in the LNN. */
    SOFTBUS_NETWORK_GET_META_NODE_INFO_ERR,                           /**< Failed to obtain the meta node information in the LNN. */

    SOFTBUS_CONN_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_CONNECT),       /**< Initial error codes of the connection module. */
    SOFTBUS_CONN_FAIL,                                                /**< Failed to set up the connection. */
    SOFTBUS_CONN_MANAGER_TYPE_NOT_SUPPORT,                            /**< Unsupported management type. */
    SOFTBUS_CONN_MANAGER_OP_NOT_SUPPORT,                              /**< Unsupported management option. */
    SOFTBUS_CONN_MANAGER_PKT_LEN_INVALID,                             /**< Invalid PKT length. */
    SOFTBUS_CONN_MANAGER_LIST_NOT_INIT,                               /**< The list is not initialized. */
    SOFTBUS_CONN_INVALID_CONN_TYPE,                                   /**< Invalid connection type. */
    SOFTBUS_CONNECTION_BASE,                                          /**<  */
    SOFTBUS_CONNECTION_ERR_CLOSED,                                    /**< Failed to close the connection. */
    SOFTBUS_CONNECTION_ERR_DRIVER_CONGEST,                            /**< The connection driver is congested. */
    SOFTBUS_CONNECTION_ERR_SOFTBUS_CONGEST,                           /**< The DSoftBus is congested. */
    SOFTBUS_CONNECTION_ERR_CONNID_INVALID,                            /**< Invalid connection ID. */
    SOFTBUS_CONNECTION_ERR_SENDQUEUE_FULL,                            /**< The connection send queue is full. */
    SOFTBUS_BRCONNECTION_POSTBYTES_ERROR,                             /**< Failed to send bytes in the BR connection. */
    SOFTBUS_BRCONNECTION_GETCONNINFO_ERROR,                           /**< Failed to obtain BR connection information. */
    SOFTBUS_BRCONNECTION_STRNCPY_ERROR,                               /**< STRNCPY error in a BR connection. */
    SOFTBUS_BRCONNECTION_PACKJSON_ERROR,                              /**< Failed to pack JSON in the BR connection. */
    SOFTBUS_BRCONNECTION_CONNECTDEVICE_MALLOCFAIL,                    /**< Failed to allocate memory for the BR connection. */
    SOFTBUS_BRCONNECTION_CONNECTDEVICE_GETSOCKETIDFAIL,               /**< Failed to obtain the socket ID in the BR connection. */
    SOFTBUS_BRCONNECTION_DISCONNECT_NOTFIND,                          /**< Failed to find the BR connection to disconnect. */
    SOFTBUS_TCPCONNECTION_SOCKET_ERR,                                 /**< TCP socket error. */
    SOFTBUS_BLECONNECTION_REG_GATTS_CALLBACK_FAIL,                    /**< Failed to register the GATTS callback in the BLE connection. */
    SOFTBUS_BLECONNECTION_ADD_SERVICE_FAIL,                           /**< Failed to add the service in the BLE connection. */
    SOFTBUS_BLECONNECTION_ADD_CHAR_FAIL,                              /**< Failed to add char in the BLE connection. */
    SOFTBUS_BLECONNECTION_ADD_DES_FAIL,                               /**< Failed to add DES in the BLE connection. */
    SOFTBUS_BLECONNECTION_NOT_INIT,                                   /**< BLE connection is not initialized. */
    SOFTBUS_BLECONNECTION_NOT_START,                                  /**< BLE connection is not started. */
    SOFTBUS_BLECONNECTION_GETCONNINFO_ERROR,                          /**< Failed to obtain BLE connection information. */
    SOFTBUS_BLECONNECTION_MTU_OVERFLOW_ERROR,                         /**< The MTU for BLE connection overflows. */
    SOFTBUS_BLECONNECTION_MUTEX_LOCK_ERROR,                           /**< BLE connection mutex error. */
    SOFTBUS_BLECONNECTION_GATT_CLIENT_NOT_SUPPORT,                    /**< The GATT client does not support the BLE connection. */
    SOFTBUS_GATTC_INTERFACE_FAILED,                                   /**< GATT client connection failed. */
    SOFTBUS_BLEGATTC_NONT_INIT,                                       /**< The BLE GATT client is not initialized. */
    SOFTBUS_BLEGATTC_NOT_READY,                                       /**< The BLE GATT client is not read. */
    SOFTBUS_GATTC_DUPLICATE_PARAM,                                    /**< Duplicate GATT client parameter. */
    SOFTBUS_GATTC_NONE_PARAM,                                         /**< No GATT client parameter. */
    SOFTBUS_BLEGATTC_NODE_NOT_EXIST,                                  /**< The BLE GATT client node does not exist. */
    SOFTBUS_BLECONNECTION_CLIENT_RECV_DISCONNECT_MSG,                 /**< The client received the BLE disconnect message. */
    SOFTBUS_BLECONNECTION_CLIENT_CONNECTED_TIMEOUT,                   /**< The BLE connection at the client timed out. */
    SOFTBUS_BLECONNECTION_CLIENT_SERVICE_SEARCHED_TIMEOUT,            /**< The client failed to find the service within the specified time for the BLE connection. */
    SOFTBUS_BLECONNECTION_CLIENT_NOTIFICATED_ONCE_TIMEOUT,            /**< The registration of the first notice for the BLE connection timed out. */
    SOFTBUS_BLECONNECTION_CLIENT_NOTIFICATED_TWICE_TIMEOUT,           /**< The registration of the second notice for the BLE connection timed out. */
    Setting the MTU for the SOFTBUS_BLECONNECTION_CLIENT_MTU_SETTED_TIMEOUT, /**< The negotiation of the MTU for the BLE connection at the client timed out. */
    SOFTBUS_BLECONNECTION_CLIENT_HANDSHAKE_TIMEOUT,                   /**< The BLE client handshake timed out. */
    SOFTBUS_BLECONNECTION_CLIENT_UPDATA_STATE_ERR,                    /**< BLE client update state error. */
    SOFTBUS_BLECONNECTION_CLIENT_CREATE_DELAY_MSG_ERR,                /**< The client failed to create the delay message for the BLE connection. */
    SOFTBUS_BLECONNECTION_CLIENT_SEARCH_SERVICES_ERR,                /**< The client failed to find the service for the BLE connection. */
    SOFTBUS_BLECONNECTION_SERVER_DISCONNECT,                          /**< BLE server is disconnected. */

    SOFTBUS_DISCOVER_ERR_BASE = SOFTBUS_ERRNO(SOFTBUS_MOD_DISCOVERY), /**< Initial error codes of the discovery module */
    SOFTBUS_DISCOVER_NOT_INIT,                                        /**< The DSoftBus SDK is not initialized. */
    SOFTBUS_DISCOVER_INVALID_PKGNAME,                                 /**< Invalid bundle name detected in the discovery interface. */
    SOFTBUS_DISCOVER_SERVER_NO_PERMISSION,                            /**< The discover server has no permission. */
    SOFTBUS_DISCOVER_MANAGER_NOT_INIT,                                /**< The DSoftBus discovery service is not initialized. */
    SOFTBUS_DISCOVER_MANAGER_ITEM_NOT_CREATE,                         /**< The discovery management project is not created. */
    SOFTBUS_DISCOVER_MANAGER_INFO_NOT_CREATE,                         /**< The discovery management information is not created. */
    SOFTBUS_DISCOVER_MANAGER_INFO_NOT_DELETE,                         /**< Failed to delete discovery management information. */
    SOFTBUS_DISCOVER_MANAGER_INNERFUNCTION_FAIL,                      /**< Failed to invoke the discovery internal function. */
    SOFTBUS_DISCOVER_MANAGER_CAPABILITY_INVALID,                      /**< Invalid device discovery capability. */
    SOFTBUS_DISCOVER_MANAGER_DUPLICATE_PARAM,                         /**< Duplicate publish ID. */
    SOFTBUS_DISCOVER_MANAGER_INVALID_PARAM,                           /**< Invalid discovery management parameter. */
    SOFTBUS_DISCOVER_MANAGER_INVALID_MEDIUM,                          /**< Invalid discovery medium. */
    SOFTBUS_DISCOVER_MANAGER_INVALID_PKGNAME,                         /**< Invalid bundle name in discovery management. */
    SOFTBUS_DISCOVER_MANAGER_INVALID_MODULE,                          /**< Invalid discovery management module. */
    SOFTBUS_DISCOVER_COAP_NOT_INIT,                                   /**< The CoAP discovery is not initialized. */
    SOFTBUS_DISCOVER_COAP_INIT_FAIL,                                  /**< CoAP initialization failed. */
    SOFTBUS_DISCOVER_COAP_MERGE_CAP_FAIL,                             /**< Failed to merge the CoAP capability. */
    SOFTBUS_DISCOVER_COAP_CANCEL_CAP_FAIL,                            /**< Failed to cancel the CoAP capability. */
    SOFTBUS_DISCOVER_COAP_REGISTER_CAP_FAIL,                          /**< Failed to register the CoAP capability. */
    SOFTBUS_DISCOVER_COAP_SET_FILTER_CAP_FAIL,                        /**< Failed to set the CoAP filter capability. */
    SOFTBUS_DISCOVER_COAP_REGISTER_DEVICE_FAIL,                       /**< Failed to register the CoAP device. */
    SOFTBUS_DISCOVER_COAP_START_PUBLISH_FAIL,                         /**< Failed to start CoAP service publishing. */
    SOFTBUS_DISCOVER_COAP_STOP_PUBLISH_FAIL,                          /**< Failed to stop CoAP service publishing. */
    SOFTBUS_DISCOVER_COAP_START_DISCOVER_FAIL,                        /**< Failed to start CoAP discovery. */
    SOFTBUS_DISCOVER_COAP_STOP_DISCOVER_FAIL,                         /**< Failed to stop CoAP discovery. */

    SOFTBUS_ERR = (-1),                                               /**< The operation fails. */
    SOFTBUS_OK = 0,                                                   /**< The operation is successful. */
};

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus  */
#endif /* __cplusplus  */
#endif /* SOFTBUS_ERRCODE_H  */
